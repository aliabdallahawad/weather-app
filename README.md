# Weather App

![image](https://i.imgur.com/ZZETXfV.gif)

**Weather app is a web application that shows the weather for 5 European cities:**
- Amsterdam, Netherlands.
- Paris, France.
- Berlin, Germany.
- Barcelona, Spain.
- Brussels, Belgium.

## Setup & Installation
- You have to install Node (v10.9).

- Clone the repo, navigate to the application folder, download the packages and start the server (follow the steps below):
```bash
~: git clone https://aliabdallahawad@bitbucket.org/aliabdallahawad/weather-app.git

~: cd weather-app

~/weather-app: npm install

~/weather-app: npm start
```

- Then, open your browser with this URL: `localhost:4200`

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).
