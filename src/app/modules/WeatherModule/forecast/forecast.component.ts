import { Component } from '@angular/core';
import { WeatherService } from 'src/app/modules/WeatherModule/weather.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CityForecasts } from 'src/app/models/city.model';
import { groupBy } from 'lodash';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent {
  // city forecasts specified by city id in route params
  public city: CityForecasts;

  // city weather list based on 
  public weathersList = null;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private weatherService: WeatherService) {
    this.weatherService.getForecastsByCityId(this.activatedRoute.snapshot.params.id)
      .subscribe(city => {
        this.city = city;
        this.weathersList = this.groupCityPerDays(city);
      }, _ => this.router.navigateByUrl('/page-not-found'));
  }

  public groupCityPerDays(city: CityForecasts) {
    return groupBy(city.list, item => item.date.split(' ')[0]);
  }
}
