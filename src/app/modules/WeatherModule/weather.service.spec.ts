import { WeatherService } from "./weather.service";
import { CityView, CityId, CityData, CityTemperature, CityWeather, CityWind } from "src/app/models/city.model";
import { defer } from "rxjs";
import { COUNTRIES } from "src/app/shared/constants";

let httpClientSpy: { get: jasmine.Spy };
let weatherService: WeatherService;

// test data
const citiesResponse = {
  list: [
    {
      sys: { country: "NL" },
      weather: [
        { id: 800, main: "Clear", description: "clear sky", icon: "01d" }
      ],
      main: { temp: 34.77, temp_min: 31.67, temp_max: 38.33 },
      wind: { speed: 3.6, deg: 80 },
      id: 2759794,
      name: "Amsterdam"
    },
    {
      sys: { country: "FR" },
      weather: [
        { id: 800, main: "Clear", description: "clear sky", icon: "01d" }
      ],
      main: { temp: 27.77, temp_min: 25.67, temp_max: 30.33 },
      wind: { speed: 3.6, deg: 80 },
      id: 2759794,
      name: "Paris"
    },
  ]
};

const cityForecastResponse = {
  list: [
    {
      main: { temp: 34.9, temp_min: 26.35, temp_max: 34.9 },
      weather: [
        { id: 801, main: "Clouds", description: "few clouds", icon: "02d" }
      ],
      wind: { speed: 3.13, deg: 95.57 },
      dt_txt: "2019-07-25 00:00:00"
    },
    {
      main: { temp: 35.15, temp_min: 28.73, temp_max: 35.15 },
      weather: [
        { id: 800, main: "Clear", description: "clear sky", icon: "01d" }
      ],
      wind: { speed: 3.34, deg: 123.9 },
      dt_txt: "2019-07-25 03:00:00"
    },
    {
      main: { temp: 33.26, temp_min: 28.99, temp_max: 33.26 },
      weather: [
        { id: 801, main: "Clouds", description: "few clouds", icon: "02d" }
      ],
      wind: { speed: 5.13, deg: 97.363 },
      dt_txt: "2019-07-25 06:00:00"
    },
    {
      main: { temp: 35.87, temp_min: 28.99, temp_max: 33.26 },
      weather: [
        { id: 800, main: "Clear", description: "clear sky", icon: "01d" }
      ],
      wind: { speed: 3.26, deg: 153.48 },
      dt_txt: "2019-07-25 09:00:00"
    }
  ],
  city: {
    id: 2759794,
    name: "Amsterdam",
    country: "NL"
  }
}

/** Create async observable that emits-once and completes
 *  after a JS engine turn */
function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

beforeEach(() => {
  // TODO: spy on other methods too
  httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
  weatherService = new WeatherService(<any>httpClientSpy);
});

it('should return mapped cities (HttpClient called once)', () => {
  httpClientSpy.get.and.returnValue(asyncData(citiesResponse));

  weatherService.getCities().subscribe(cities => {
    expect(cities.length).toEqual(2);
    citiesResponse.list.map((item, index) => {
      expect(cities[index].id.value).toEqual(item.id);
      expect(cities[index].data.name).toEqual(item.name);
      expect(cities[index].data.country.code).toEqual(item.sys.country);
      expect(cities[index].data.country.name).toEqual(COUNTRIES[item.sys.country]);
      expect(cities[index].weather.main).toEqual(item.weather[0].main);
      expect(cities[index].weather.description).toEqual(item.weather[0].description);
      expect(cities[index].weather.icon).toEqual(item.weather[0].icon);
      expect(cities[index].temperature.current).toEqual(Math.round(item.main.temp));
      expect(cities[index].temperature.min).toEqual(Math.round(item.main.temp_min));
      expect(cities[index].temperature.max).toEqual(Math.round(item.main.temp_max));
      expect(cities[index].wind.speed).toEqual(item.wind.speed);
      expect(cities[index].wind.degree).toEqual(item.wind.deg);
    });
  });
  expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
});

it('should return mapped city selected by id (HttpClient called once)', () => {
  httpClientSpy.get.and.returnValue(asyncData(cityForecastResponse));

  weatherService.getForecastsByCityId(cityForecastResponse.city.id).subscribe(city => {
    expect(city.id.value).toEqual(cityForecastResponse.city.id);
    expect(city.data.name).toEqual(cityForecastResponse.city.name);
    expect(city.data.country.code).toEqual(cityForecastResponse.city.country);
    expect(city.data.country.name).toEqual(COUNTRIES[cityForecastResponse.city.country]);
    cityForecastResponse.list.map((item, index) => {
      expect(city.list[index].date).toEqual(item.dt_txt);
      expect(city.list[index].weather.main).toEqual(item.weather[0].main);
      expect(city.list[index].weather.description).toEqual(item.weather[0].description);
      expect(city.list[index].weather.icon).toEqual(item.weather[0].icon);
      expect(city.list[index].temperature.current).toEqual(Math.round(item.main.temp));
      expect(city.list[index].temperature.min).toEqual(Math.round(item.main.temp_min));
      expect(city.list[index].temperature.max).toEqual(Math.round(item.main.temp_max));
      expect(city.list[index].wind.speed).toEqual(item.wind.speed);
      expect(city.list[index].wind.degree).toEqual(item.wind.deg);
    });
  });
  expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
});
