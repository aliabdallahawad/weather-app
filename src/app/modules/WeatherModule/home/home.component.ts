import { Component } from '@angular/core';
import { WeatherService } from 'src/app/modules/WeatherModule/weather.service';
import { CityView } from 'src/app/models/city.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  // list of cities
  public cities: CityView[] = [];

  constructor(private weatherService: WeatherService) {
    // fetch the list of cities and pass it to private variable (cities) to be rendered
    this.weatherService.getCities()
      .subscribe(cities => this.cities = cities);
  }
}
