import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { HomeComponent } from './home/home.component';
import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherService } from './weather.service';
import { ComponentsModule } from './../../components/components.module';
import { CommonModule } from '@angular/common';
import { WeatherHttpInterceptor } from '../../interceptors/weather.interceptor';
import { ForecastComponent } from './forecast/forecast.component';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    HomeComponent,
    ForecastComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ComponentsModule,
    WeatherRoutingModule,
    MomentModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: WeatherHttpInterceptor, multi: true },
    WeatherService
  ]
})
export class WeatherModule { }
