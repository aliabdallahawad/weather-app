import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityView, CityForecasts } from 'src/app/models/city.model';
import { entites } from './../../shared/entites';

@Injectable()
export class WeatherService {

  /**
   * @param http - Http Client
   */
  constructor(private http: HttpClient) { }

  /**
   * getCities - retrieve list of cities with current weather status
   * @returns list of city objects
   */
  public getCities(): Observable<CityView[]> {
    return this.http.get<{list: any[]}>(entites.weather.list([2759794,3128760,2988507,2800866,2950159]))
      .pipe(map(response => response.list.map(city => CityView.mapResponse(city))));
  }

  /**
   * getForecastsByCityId - retrieve list of weather forests for next (5 days / 3 hours per day) for specific city
   * @param cityId - selected city id
   * @returns city with array of weather list
   */
  public getForecastsByCityId(cityId: number): Observable<CityForecasts> {
    return this.http.get(entites.weather.forecast(cityId))
      .pipe(map(response => CityForecasts.mapResponse(response)));
  }
}