import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-body',
  template: ''
})
export class CardBodyMockComponent {
  @Input() public location: string;
  @Input() public temperature: number;
  @Input() public description: string;
  @Input() public icon: string;
  @Input() public strength: number;
}
