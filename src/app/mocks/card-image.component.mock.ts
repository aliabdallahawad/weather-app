import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-image',
  template: ''
})
export class CardImageMockComponent {
  @Input() public image: string;
  @Input() public alt: string;
}
