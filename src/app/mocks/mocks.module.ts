import { NgModule } from '@angular/core';
import { LayoutMockComponent } from './layout.component.mock';
import { CardBodyMockComponent } from './card-body.component.mock';
import { CardImageMockComponent } from './card-image.component.mock';

@NgModule({
  declarations: [
    LayoutMockComponent,
    CardBodyMockComponent,
    CardImageMockComponent
  ],
  exports: [
    LayoutMockComponent,
    CardBodyMockComponent,
    CardImageMockComponent
  ]
})
export class MocksModule { }
