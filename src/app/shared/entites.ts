import { environment } from '../../environments/environment';

export const entites = {
  weather: {
    list: (citiesIds: number[]) => `${environment.apiURL}/group?id=${citiesIds.toString()}&units=metric`,
    forecast: (cityId: number) => `${environment.apiURL}/forecast?id=${cityId}&units=metric`
  }
};
