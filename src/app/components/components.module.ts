import { NgModule } from '@angular/core';

import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { CardImageComponent } from './card/card-image/card-image.component';
import { CardBodyComponent } from './card/card-body/card-body.component';
import { CardBodyLocationComponent } from './card/card-body/card-body-location/card-body-location.component';
import { CardBodyWeatherComponent } from './card/card-body/card-body-weather/card-body-weather.component';
import { CardBodyWindStrengthComponent } from './card/card-body/card-body-wind-strength/card-body-wind-strength.component';
import { RouterModule } from '@angular/router';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';
import { MomentModule } from 'angular2-moment';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MomentModule
  ],
  declarations: [
    HeaderComponent,
    LayoutComponent,
    JumbotronComponent,
    CardComponent,
    CardImageComponent,
    CardBodyComponent,
    CardBodyLocationComponent,
    CardBodyWeatherComponent,
    CardBodyWindStrengthComponent,
    WeatherDetailsComponent,
  ],
  exports: [
    LayoutComponent,
    JumbotronComponent,
    CardComponent,
    WeatherDetailsComponent
  ]
})
export class ComponentsModule { }
