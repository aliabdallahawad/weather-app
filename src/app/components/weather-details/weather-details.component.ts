import { Component, Input } from '@angular/core';
import { CityWeatherListItem } from 'src/app/models/city.model';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss']
})
export class WeatherDetailsComponent {
  // background color of jumbotron
  @Input()
  public list: CityWeatherListItem[] = null;
}
