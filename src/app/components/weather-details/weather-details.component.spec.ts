import { TestBed, async } from '@angular/core/testing';
import { WeatherDetailsComponent } from './weather-details.component';
import { CityTemperature, CityWeather, CityWind } from 'src/app/models/city.model';
import { Pipe, PipeTransform } from '@angular/core';

describe('WeatherDetailsComponent', () => {
  const list = [
    {
      temperature: new CityTemperature(25, 23, 27),
      weather: new CityWeather('Sunny', 'Clear Sky', 'sunny-icon'),
      wind: new CityWind(2.5, 190),
      date: '2019-07-29 14:00:00'
    }
  ];

  // mocking moment pipe
  @Pipe({name: 'amDateFormat'})
  class MomentMockPipe implements PipeTransform {
    transform(value: number): number {
      //Do stuff here, if you want
      return value;
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        WeatherDetailsComponent,
        MomentMockPipe,
      ]
    }).compileComponents();
  }));

  it('should create the weather details', () => {
    const fixture = TestBed.createComponent(WeatherDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render only one element of the list`, () => {
    const fixture = TestBed.createComponent(WeatherDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    app.list = list;

    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('li').length).toEqual(1);
  });

  it(`should render weather icon`, () => {
    const fixture = TestBed.createComponent(WeatherDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    app.list = list;

    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img').src).toEqual(`https://openweathermap.org/img/wn/${list[0].weather.icon}@2x.png`);
  });

  it(`should render weather stats (temperature, description, min & max, wind strength)`, () => {
    const fixture = TestBed.createComponent(WeatherDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    app.list = list;

    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.temp').textContent).toContain(list[0].temperature.current);
    expect(compiled.querySelector('.description').textContent).toEqual(list[0].weather.description);
    expect(compiled.querySelector('.min-max-temp').textContent).toContain(list[0].temperature.min);
    expect(compiled.querySelector('.min-max-temp').textContent).toContain(list[0].temperature.max);
    expect(compiled.querySelector('.wind-strength').textContent).toContain(list[0].wind.speed);
  });
});
