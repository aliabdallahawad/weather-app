import { TestBed, async } from '@angular/core/testing';
import { CardBodyWeatherComponent } from './card-body-weather.component';

describe('CardBodyWeatherComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardBodyWeatherComponent
      ]
    }).compileComponents();
  }));

  it('should create the card body weather', () => {
    const fixture = TestBed.createComponent(CardBodyWeatherComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render weather icon based on icon from inputs`, () => {
    const fixture = TestBed.createComponent(CardBodyWeatherComponent);
    const app = fixture.debugElement.componentInstance;
    app.icon = 'icon1';
    // detect component changes that reflect to html view
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img').src).toEqual('https://openweathermap.org/img/wn/icon1@2x.png');
  });

  it(`should render weather temperature and description based on inputs`, () => {
    const fixture = TestBed.createComponent(CardBodyWeatherComponent);
    const app = fixture.debugElement.componentInstance;
    app.temperature = 23;
    app.description = 'Clear Sky';
    // detect component changes that reflect to html view
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.degree').textContent).toEqual('23°C');
    expect(compiled.querySelector('.description').textContent).toEqual('Clear Sky');
  });
});
