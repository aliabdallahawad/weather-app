import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-body-weather',
  templateUrl: './card-body-weather.component.html',
  styleUrls: ['./card-body-weather.component.scss']
})
export class CardBodyWeatherComponent {
  // current weather temperature
  @Input()
  public temperature: number = null;

  // current weather description (sunny, raining, ...)
  @Input()
  public description: string = '';

  // weather icon id based on openweather icons (https://openweathermap.org/weather-conditions)
  @Input()
  public icon: string = '';
}
