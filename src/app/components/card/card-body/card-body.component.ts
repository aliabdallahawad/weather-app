import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-body',
  templateUrl: './card-body.component.html',
  styleUrls: ['./card-body.component.scss']
})
export class CardBodyComponent {
  // card location
  @Input()
  public location: string = '';

  // card temperature
  @Input()
  public temperature: number = null;

  // card temperature description
  @Input()
  public description: string = '';

  // card icon
  @Input()
  public icon: string = '';

  // wind strength
  @Input()
  public strength: number = null;
}
