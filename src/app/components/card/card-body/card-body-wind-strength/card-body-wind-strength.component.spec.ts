import { TestBed, async } from '@angular/core/testing';
import { CardBodyWindStrengthComponent } from './card-body-wind-strength.component';

describe('CardBodyWindStrengthComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardBodyWindStrengthComponent
      ]
    }).compileComponents();
  }));

  it('should create the card body wind strength', () => {
    const fixture = TestBed.createComponent(CardBodyWindStrengthComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render wind icon`, () => {
    const fixture = TestBed.createComponent(CardBodyWindStrengthComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img').src).toContain('assets/icons/wind.svg');
  });

  it(`should render wind strength based on input`, () => {
    const fixture = TestBed.createComponent(CardBodyWindStrengthComponent);
    const app = fixture.debugElement.componentInstance;
    app.strength = 3.1;
    // detect component changes that reflect to html view
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.card-wind-strength').textContent).toEqual('3.1');
  });
});
