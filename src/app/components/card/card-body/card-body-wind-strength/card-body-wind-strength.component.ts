import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-body-wind',
  templateUrl: './card-body-wind-strength.component.html',
  styleUrls: ['./card-body-wind-strength.component.scss']
})
export class CardBodyWindStrengthComponent {
  // current wind strength
  @Input()
  public strength: number = null;
}
