import { TestBed, async } from '@angular/core/testing';
import { CardBodyLocationComponent } from './card-body-location.component';

describe('CardBodyLocationComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardBodyLocationComponent
      ]
    }).compileComponents();
  }));

  it('should create the card body location', () => {
    const fixture = TestBed.createComponent(CardBodyLocationComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render location icon and location as 'Amsterdam, Netherlands'`, () => {
    const fixture = TestBed.createComponent(CardBodyLocationComponent);
    const app = fixture.debugElement.componentInstance;
    app.location = 'Amsterdam, Netherlands';
    // detect component changes that reflect to html view
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('img').src).toContain('assets/icons/location.svg');
    expect(compiled.querySelector('.card-location-name').textContent).toContain('Amsterdam, Netherlands');
  });
});
