import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-body-location',
  templateUrl: './card-body-location.component.html',
  styleUrls: ['./card-body-location.component.scss']
})
export class CardBodyLocationComponent {
  @Input()
  public location: string = '';
}
