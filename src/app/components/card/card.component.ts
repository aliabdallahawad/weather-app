import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html'
})
export class CardComponent {
  // city id
  @Input()
  public id: number = null;

  // image url
  @Input()
  public image: string = '';

  // location
  @Input()
  public location: string = '';

  // current temperature
  @Input()
  public temperature: number = null;

  // current temperature description
  @Input()
  public description: string = '';

  // weather icon
  @Input()
  public icon: string = '';

  // wind strength
  @Input()
  public windStrength: number = null;

  constructor(private router: Router) { }

  public navigateToForecast() {
    this.router.navigateByUrl(`city/${this.id}/forecast`);
  }
}
