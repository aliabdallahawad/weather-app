import { TestBed, async } from '@angular/core/testing';
import { CardComponent } from './card.component';
import { CardImageMockComponent } from '../../mocks/card-image.component.mock';
import { CardBodyMockComponent } from '../../mocks/card-body.component.mock';
import { RouterTestingModule } from '@angular/router/testing';

describe('CardComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardComponent,
        CardImageMockComponent,
        CardBodyMockComponent
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));

  it('should create the card', () => {
    const fixture = TestBed.createComponent(CardComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should card be clickable and call navigateToForecast`, () => {
    // create card component
    const fixture = TestBed.createComponent(CardComponent);
    const app = fixture.debugElement.componentInstance;
    // spy on navigateToForecast method to be checked in expect method
    spyOn(app, 'navigateToForecast');
    const compiled = fixture.debugElement.nativeElement;
    compiled.querySelector('.card').click();
    // detect changes
    fixture.detectChanges();
    expect(app.navigateToForecast).toHaveBeenCalled();
  });

  it(`should navigate to forecast url when calling navigateToForecast`, () => {
    // test data
    const cityId = 231382;
    // create card component
    const fixture = TestBed.createComponent(CardComponent);
    const app = fixture.debugElement.componentInstance;
    app.id = cityId;
    spyOn(app.router, 'navigateByUrl');
    app.navigateToForecast();
    expect(app.router.navigateByUrl).toHaveBeenCalledWith(`city/${cityId}/forecast`);
  });
});
