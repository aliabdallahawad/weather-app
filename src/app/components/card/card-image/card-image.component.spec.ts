import { TestBed, async } from '@angular/core/testing';
import { CardImageComponent } from './card-image.component';

describe('CardImageComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CardImageComponent
      ]
    }).compileComponents();
  }));

  it('should create the card image', () => {
    const fixture = TestBed.createComponent(CardImageComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render image with passed src and alt values`, () => {
    // test data
    const image = 'https://www.example.com/assets/example.jpg';
    const alt = 'alternative text';
    
    // creating component
    const fixture = TestBed.createComponent(CardImageComponent);
    const app = fixture.debugElement.componentInstance;
    app.image = image;
    app.alt = alt;
    // detect component changes that reflect to html view
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const imgSelector = compiled.querySelector('img');
    expect(imgSelector.src).toEqual(image);
    expect(imgSelector.alt).toEqual(alt);
  });
});
