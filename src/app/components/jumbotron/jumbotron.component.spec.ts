import { TestBed, async } from '@angular/core/testing';
import { JumbotronComponent } from './jumbotron.component';

describe('JumbotronComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        JumbotronComponent
      ]
    }).compileComponents();
  }));

  it('should create the jumbotron', () => {
    const fixture = TestBed.createComponent(JumbotronComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should set background color based on input`, () => {
    const fixture = TestBed.createComponent(JumbotronComponent);
    const app = fixture.debugElement.componentInstance;
    app.backgroundColor = 'rgb(211, 211, 211)';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('section').style.backgroundColor).toEqual('rgb(211, 211, 211)');
  });
});
