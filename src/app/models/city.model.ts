import { COUNTRIES } from './../shared/constants';

export class CityId {
  // city id value
  value: number = null;

  constructor(value: number) {
    this.value = value;
  }
}

export class CityData {
  // city name (Amsterdam, Paris, ...)
  name: string = null;
  // city country code (NL, FR, ...) mapped to country name (Netherlands, France, ...)
  country: {
    code: string,
    name: string
  } = null;

  constructor(name: string, countryCode: string) {
    this.name = name;
    this.country = {
      code: countryCode,
      name: COUNTRIES[countryCode] || countryCode
    }
  }
}

export class CityTemperature {
  // current temperature
  current: number;
  // min temperature
  min: number;
  // max temperature
  max: number

  constructor(current: number, min: number, max: number) {
    this.current = Math.round(current);
    this.min = Math.round(min);
    this.max = Math.round(max);
  }
}

export class CityWeather {
  // main weather description ('cloud', 'sunny', ...)
  main: string;
  // detailed weather description ('clear sky', ...)
  description: string;
  // weather icon code
  icon: string

  constructor(main: string, description: string, icon: string) {
    this.main = main;
    this.description = description;
    this.icon = icon;
  }
}

export class CityWind {
  // wind speed
  speed: number;
  // wind direction degree
  degree: number;

  constructor(speed: number, degree: number) {
    this.speed = speed;
    this.degree = degree;
  }
}

/**
 * CityView contains city id, data and current weather, temperature and wind data
 */
export class CityView {
  id: CityId;
  data: CityData;
  temperature: CityTemperature;
  weather: CityWeather;
  wind: CityWind;

  constructor(id: CityId, data: CityData, temperature: CityTemperature, weather: CityWeather, wind: CityWind) {
    this.id = id;
    this.data = data;
    this.temperature = temperature,
    this.weather = weather,
    this.wind = wind
  }

  /**
   * map response object from API to be filled in CityView object
   * @param data - response object from APIs
   * @returns CityView object filled from response object
   */
  public static mapResponse(data: any): CityView {
    return new CityView(
      new CityId(data.id),
      new CityData(data.name, data.sys.country),
      new CityTemperature(data.main.temp, data.main.temp_min, data.main.temp_max),
      new CityWeather(data.weather[0].main, data.weather[0].description, data.weather[0].icon),
      new CityWind(data.wind.speed, data.wind.deg)
    );
  }
}

/**
 * city weather list item that holds temperature, weather, wind and date
 */
export class CityWeatherListItem {
  temperature: CityTemperature;
  weather: CityWeather;
  wind: CityWind;
  date: string;

  constructor(temperature: CityTemperature, weather: CityWeather, wind: CityWind, date: string) {
    this.temperature = temperature;
    this.weather = weather;
    this.wind = wind;
    this.date = date;
  }
}

/**
 * CityForecasta contains city id, data and list of weather, temperature and wind data for 5 days / 3 hours per day
 */
export class CityForecasts {
  id: CityId;
  data: CityData;
  list: CityWeatherListItem[];

  constructor(id: CityId, data: CityData, list: CityWeatherListItem[]) {
    this.id = id;
    this.data = data;
    this.list = list;
  }

  /**
   * map response object from forecasts API to be filled in CityForecasts object
   * @param data - response object from APIs
   * @returns CityForecasts object filled from response object
   */
  public static mapResponse(data: any): CityForecasts {
    return new CityForecasts(
      new CityId(data.city.id),
      new CityData(data.city.name, data.city.country),
      data.list.map(item => new CityWeatherListItem(
        new CityTemperature(item.main.temp, item.main.temp_min, item.main.temp_max),
        new CityWeather(item.weather[0].main, item.weather[0].description, item.weather[0].icon),
        new CityWind(item.wind.speed, item.wind.deg),
        item.dt_txt
      ))
    );
  }
}
